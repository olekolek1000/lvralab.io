---
title: Resonite
weight: 50
---

# Resonite

{{% hint info %}}

**Note**: The Linux-native version of Resonite, amid deprioritization of developer time to fix its numerous issues, was put on indefinite hold in August 2024. This document only covers running under Proton.

{{% /hint %}}

{{% hint warning %}}

Despite the above deprecation, Steam will still download the native Linux binary by default.

Make sure you have a compatibility tool selected (e.g. GE-Proton-RTSP) before launching Resonite.

{{% /hint %}}

The current recommended Proton variant is [**GE-Proton-RTSP**](https://github.com/SpookySkeletons/proton-ge-rtsp/releases).

OpenComposite should now provide a close-to-flawless experience on most controllers and when using hand tracking.

## Visual bugs

There are some minor visual bugs specific to running Resonite on Proton, such as textures taking longer to load and appearing black for a few seconds when close to the player.

## Crashes

Resonite has been seen causing random SteamVR crashes on Linux before. In some situations the engine also tends to freeze. On OpenComposite, the game will rarely close out on its own without leaving any obvious trace of things going wrong &mdash; this is being investigated.

The cause of these issues and whether they are Proton-specific isn't currently known.

## Performance

In larger sessions Resonite is typically CPU-bound, there isn't much that can be done besides upgrading your CPU. In situations where the GPU is a bottleneck, lowering the resolution <!-- TODO link to resolution scaling --> (either in the OpenXR runtime or through OpenComposite) may help dramatically.

## Crash on loading into a world with video players

On GE-Proton9-10 (for example), loading into a world with e.g. a YouTube video or live stream can crash the game. Try using the latest GE-Proton-RTSP.

## Controls busted after switching to desktop

Resonite allows a VR player to take a break and switch to desktop mode by pressing the F8 key.

Because controller hot-swapping is not yet implemented in Monado & OpenComposite, turning off a controller will freeze its state, including buttons pressed and analog stick input. Resonite will continue processing VR inputs even after switching to desktop.

The end result is that desktop controls can act strangely. For example, if you were holding the grab sensor as you turned off the VR controller, picking up items or inspector windows stops working with the mouse.
