---
title: Performance
weight: 46
---

# Performance

## A word on antialiasing

Antialiasing should be avoided when possible and the compositor scale should be increased in monado to smooth out edges. In general AA is found to increase the latency of delivered frames quite noticably and the GPU utilization far more than simple super sample of the actual VR session or app.

## Set AMD GPU power profile mode

AMD GPUs will attempt to power save in between rendering frames, for flatscreen games this is helpful, but for VR this is quite negatively impactful on the VR compositor's ability to timewarp frames so the user's viewport does not stutter or cause sickness.

This is very important to avoid stuttering, do not skip this step if you use AMD.

### The simple way: Use CoreCtrl

- Install [CoreCtrl](https://gitlab.com/corectrl/corectrl)
- Select your GPU on the top
- Set Performance mode to Advanced
- Set Power profile to VR
- Set the GPU and Memory sliders to max

### Enable VR profile using a script

```bash
#!/usr/bin/env bash

# Enable manual override
echo "manual" | sudo tee /sys/class/drm/card0/device/power_dpm_force_performance_level

# Translate "VR" into profile number
vr_profile=$(cat /sys/class/drm/card0/device/pp_power_profile_mode | grep ' VR ' | awk '{ print $1; }')

# Set profile to VR
echo $vr_profile | sudo tee /sys/class/drm/card0/device/pp_power_profile_mode
```

### Disable VR profile using a script
```bash
#!/usr/bin/env bash

# Disable manual override
echo "auto" | sudo tee /sys/class/drm/card0/device/power_dpm_force_performance_level

# Set profile to DEFAULT
echo 0 | sudo tee /sys/class/drm/card0/device/pp_power_profile_mode
```


# Did all of this and still suffering stuttering issues, discomfort, or sickness?

You may need to increase the overhead of your compositor timewarp. Some GPUs cannot effectively meet the demands of realtime reprojection on the default value of 4 miliseconds so a safer value of 8 can be recommended if your symptoms are accompanied by 100% GPU usage:

`U_PACING_COMP_MIN_TIME_MS=8`

Insert this enviornment variable into your monado-service launch or into your envision profile and simply start the runtime without a rebuild. Bump the value up, as appropriate, based on the absolute power of your GPU.

Lower spec users such as iGPUs and more should consider radically higher values here, experiment in the double digits to arrive at a number that works well from your perspective.