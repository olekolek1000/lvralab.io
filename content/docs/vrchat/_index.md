---
title: VRChat
weight: 50
---

# VRChat

The most popular social VR game, here are resources to get the best experience on Linux.

## Recommended Proton

Current recommended Proton: [Proton-GE-RTSP](https://github.com/SpookySkeletons/proton-ge-rtsp/releases)

As opposed to the default setting, this Proton version enables more stable and feature rich video playback. Featuring a number of fixes and most prominently playback of livestreamed content, typically real time streaming protocol found at live events and often utilized by the VRCDN.

As of the GE-Proton9-10-rtsp14 release, video content should be working correctly. Please report any notable stability bugs to LVRA general chat or by creating an issue on GitHub.

This tar file can be extracted to a ~/.steam/steam/compatibilitytools.d/ (create if it does not already exist) folder and steam restarted to set the game proton version to rtsp.

## Common issues

If you utilize pipewire as your audio server, VRChat has a tendancy to drop multiple seconds of audio over displayport connections under load for HMDs like the Valve Index [this workaround is required not to drop audio from time to time](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Troubleshooting#stuttering-audio-in-virtual-machine).

Similar tuning of pulseaudio can fix the issue if encountered there.

Should the game prompt to exit the game upon an anticheat failure, simply try to load the game again until it works. [See EAC section](eac/)

Given a [few caveats](video_players/) most video players will work.

VRChat drags quite a tangle of privacy concerns.
Please consider your opsec in relation to all VR API input including poses, all voice communication, unsandboxed code execution, the EAC anticheat system which occasionally probes system proc info and much more, and the game maintaining a memory buffered audio video recording of the window contents tied to the report system.