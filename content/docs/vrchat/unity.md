---
weight: 900
title: Unity
---

# Unity

This article will take you through installing Unity Hub, Unity, and ALCOM (the FOSS alternative to VRChat Creator Companion).

## Set up UnityHub

Download the [UnityHub AppImage](https://public-cdn.cloud.unity3d.com/hub/prod/UnityHub.AppImage). Run the AppImage like you normally would.

Running UnityHub from [Flatpak](https://flathub.org/apps/com.unity.UnityHub) is also still an option, but it seems ALCOM has better integration with the AppImage.

First, take a look at this page from [the official VRChat docs](https://creators.vrchat.com/sdk/upgrade/unity-2022/#using-the-unity-hub) about how to install the supported Unity version from UnityHub. We'll do something similar.

1. Open Unity Hub and sign in with your Unity account.
1. Install **2022.3.22** (VRChat's current supported Unity version, at the time of writing) by running `./UnityHub.AppImage unityhub://2022.3.22f1/887be4894c44`.
1. When prompted, select *Android Build Support* and *Windows Build Support (Mono)*.
    - You may need to install `cpio` from your distro's repos in order to install these components (The AppImage erroneously doesn't include it). To check if it's installed, run `which cpio` in a terminal.

## ALCOM

vrc-get is a native FOSS package manager written in Rust. Its GUI counterpart was formerly known as vrc-get-gui, but has been rebranded as ALCOM (***Al***ternative Creator ***Com***panion).

1. Download the [AppImage from the latest "gui" release](https://github.com/vrc-get/vrc-get/releases). Pick the file named `alcom-<some version>-x86_64.AppImage`.
1. Run the AppImage.
1. In the settings tab, under Unity Hub Path, point it to your UnityHub.AppImage.
1. Create a new project and open it.
1. Go to VRChat SDK -> Show Control Panel and dock it to the main window.
1. It may open windows partially offscreen. If this happens, see [this section below](#editor-windows-appear-partially-offscreen).

Now you should be able to create and upload avatars and worlds like you normally would.

<!--
This is no longer maintained.

## Option 2. LinuxCreatorCompanion

If you need some of the GUIs, it is now possible to use VRChat Creator Companion (VCC) on Linux.

The official VRChat Creator Companion uses Webview2 (Microsoft Edge). This component is difficult to install, and doesn't run natively. Instead, you can use LinuxCreatorCompanion.

[GitHub repo for LinuxCreatorCompanion](https://github.com/RinLovesYou/LinuxCreatorCompanion)

It works by using wine to extract the necessary bits to re-embed into a native client application. While experimental, you can enjoy much easier avatar, world, and asset creation. Again, this is experimental, and we discourage you from trying this on important projects. **Remember to make frequent project backups.** Please leave any feedback about bugs or ideas on the GitHub issues tab.

To install, click the releases tab, download the zip, extract it somewhere, and run `./LinuxCreatorCompanion`.

**Note:** If you're using Unity Hub via flatpak, it will have trouble finding and opening Unity. For now, create this wrapper script, chmod +x it, and put it somewhere in your PATH, like `/usr/local/bin/unityhub`.

```bash
#!/usr/bin/env bash
exec flatpak run com.unity.UnityHub "$@"
```

-->

## World & Avatar SDK

The VRChat Worlds SDK fails to build the assetbundle and Build and Test doesn't work for either SDK.

> ⚠️ **WARNING** \
> This modifies the VRChat SDK using [Harmony](https://github.com/pardeike/Harmony) to properly work on Linux. \
> This is directly against the VRChat Terms of Service.

Patches for both the Worlds and Avatars SDK are available: https://github.com/BefuddledLabs/LinuxVRChatSDKPatch ([VCC](https://befuddledlabs.github.io/LinuxVRChatSDKPatch/))

## Troubleshooting

### Unable to activate Unity license

If you are using a Fedora or RHEL-based distribution, this may be caused by an OpenSSL configuration option.

To verify that this is the problem, check your `~/.config/unity3d/Unity/Unity.Licensing.Client.log`.
If you see the text `Interop+Crypto+OpenSslCryptographicException: error:03000098:digital envelope routines::invalid digest`, running the following command as root will re-enable SHA-1 cryptography and resolve the issue:

`update-crypto-policies --set DEFAULT:SHA1`

For more information, see the RHEL documentation on [security hardening](https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/9/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening#proc_re-enabling-sha-1_using-the-system-wide-cryptographic-policies).

### Shaders fail to include some .cginc files

This can happen with Poiyomi. It's caused by case-sensitivity. You may need to edit the shader files directly. Open them in any text editor, and correct the casing of filenames.

### Editor windows appear partially offscreen

Unity may open undocked panel windows (such as color pickers and unitypackage import dialogs) partially offscreen.

Some workarounds:
- For KDE, hold the Meta (AKA Super, AKA WinKey) key and click anywhere in the window to drag it back onscreen.
- Alt-Tab to the specific window, then press Meta-Right to snap the window to the right side of the screen.
- For KDE, press ALT+F3 -> More Actions -> Move.

### Can't drag-and-drop files from external programs

Rather than using Assets -> Import New Asset or Import Package, it's normally possible to drag-and-drop a unitypackage or other asset from a file browser directly into Unity's Project pane, where it will be copied to the Assets folder.

However, if your file browser is running on Wayland, this will show "Failed copying file" errors.

See issue [flathub/com.unity.UnityHub#97](https://github.com/flathub/com.unity.UnityHub/issues/97).

As a workaround, launch your file browser in Xwayland mode. Unity will accept the file as it should.

```
WAYLAND_DISPLAY="" dolphin
```

### When Unity is opened via ALCOM, it crashes when dragging a hierarchy object

{{% hint info %}}

**Note**: If you are on Nvidia, see the [Crashing frequently with Nvidia](#crashing-frequently-with-nvidia) section first.

{{% /hint %}}

An ALCOM-launched instance of Unity can crash after trying to drag-and-drop an item (within Unity, ie. not external programs).

This [issue #1346](https://github.com/vrc-get/vrc-get/issues/1346) was fixed by ALCOM v0.1.14, so just update.

Analysis: ALCOM has a different environment than the rest of your system, mostly by virtue of being an AppImage. This means the Unity subprocess that it spawns was implicitly inheriting its crummy vars. Unfun fact: This environment inheritance causes several other problems (such as ignoring the user's GTK theme for menubars and context menus, especially on a KDE system), but I'm not sure the cleanest way to unset those vars yet. A wrapper script around the Unity binary?

### VRCFury builds lock up the editor

Maybe due to the windows offscreen issue mentioned above, when starting build or play mode, the VRCFury progress window may initially be invisible, or pop underneath, which gives the *appearance* that the build isn't doing anything and has locked up. Use the "Alt-tab to the specific window then Meta-Right" trick to get the VRCFury progress window to appear onscreen again. That window may also be solid black, but this doesn't necessarily mean the build has frozen.

### Moving file failed

A dialog may appear upon trying to build an avatar, where none of the buttons are helpful:

> Moving Temp/unitystream.unity3d to /tmp/DefaultCompany/Some Avatar Project/prefab-id-v1\_avtr\_00000000-0000-0000-0000-000000000000_0000000000.prefab.unity3d:
>
> Force Quit | Try Again | Cancel

This seems to happen consistently if the projects are stored on an EXFAT partition. Consider using another filesystem, such as NTFS if you still need projects accessible under both Windows and Linux.

### Crashing frequently with Nvidia

On some systems running Nvidia graphics, the Unity Editor may crash frequently from common actions like importing a unitypackage or dragging a file.

If you launch projects from ALCOM (v0.1.13 or newer): Open ALCOM, go to the "Default Unity Command-line Arguments" setting on the Settings page. Click "Edit", "Customize", "Reset", "+", then type in `-force-vulkan-layers` into the new box, then click "Save".

If you launch projects from Unity Hub: Open Unity Hub, click the `...` icon to the right of your project, then click "Add command line arguments", and add the argument `-force-vulkan-layers`.

Alternatively, downgrading the Nvidia driver to version 545 may also fix the problem.

<!-- This fix is mentioned in https://discussions.unity.com/t/unity-editor-crashes-frequently-on-linux-vulkan-after-updating-gpu-drivers/941387/10, and appears to be a new issue as of driver version 550 -->