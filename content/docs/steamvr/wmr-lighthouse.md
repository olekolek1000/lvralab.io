---
title: WMR Lighthouse
weight: 9000
---

# Running WMR with Lighthouse on SteamVR

This is a guide on how to use WMR as a display source and lighthouse as tracking system for WMR on Linux to use with SteamVR.

It will not utilise space calibration between basalt tracking and lighthouse systems as at the moment of writing, Basalt on WMR is drifting, and will crash at any point with poor tracking quality (which it might do frequently if left unattended).

Instead, in this guide we will override head/camera in steamvr so that tracker will be used as one.

## Why?

Because WMR at the moment might be the only viable native pcvr headset that isn't index, costs dirt cheap and has the most features that Monado can support for later use. Overall if you have vive and want higher resolution while not buying something costly, buying WMR, vive tracker (or tundra) for such purpose might just be a good option.

Otherwise, it's just fun to see how far you can stretch definition of *jank vr setup* but actually kind of useful.

## Prerequisites

1. Working Monado with WMR (this guide is aimed towards using [Envision](/docs/fossvr/envision/) for that)
2. Working SteamVR lighthouse system
   - For head tracker, you can use any lighthouse equipment you have, it doesn't matter, but preferably it would be a small tracker, like a tundra one, so you can use it for more comfortable experience
   - You need to connect your tracker at least once into SteamVR to be recognized in next steps for serial number
3. Getting serial number of your desired head tracker
   - You can get it from `~/.steam/steam/config/lighthouse/lighthousedb.json`. Inside it there would be `known_objects` section which contains all lighthouse devices. Find one that looks like correct device and copy it's `serialNumber`

## Envision

It's preferrable to create a separate WMR profile in Envision with the following changes compared to the default WMR profile:

- disable Basalt
- disable pull on build
- use a different prefix
- use a different Monado source directory (technically optional, but this way it won't mess with your main prefixes since we will commit into those source repositories)

## Monado

You would need to make sure that WMR headset is the only one being initialised in Monado. In my case it wasn't, because i used my Vive as dongles for my knuckles, so i had to comment out vive driver from Monado completely.

If using envision, unset Basalt toggle in profile settings to prevent it from being built.

**If you're using steamvr dongles for knuckles and tracker, you don't need to do next steps bellow for monado. Also steps might be inaccurate as monado changes code around lighthouse builders.**

There's probably a better way to do this rather than editing the Monado source directly (if you as the reader know a better way, make sure to contribute it here).

For that (assuming default locations, adjust if necessary), navigate to your Monado source directory your Envision profile uses, the default would be: `~/.local/share/envision/monado`. Open CMakeLists.txt and:

Comment (with `#`) 3 lines in final list of compiled drivers:

1. `"SURVIVE"`
2. `"VIVE"`
3. `"STEAMVR_LIGHTHOUSE"`

And rebuild Monado, either manually or by using Envision with clean build.

## SteamVR

For starters, we need to register Monado as driver for SteamVR:

Assuming default installation for both Steam and Monado inside Envision, the command would be: `~/.steam/steam/steamapps/common/SteamVR/bin/vrpathreg.sh adddriver ~/.local/share/envision/prefixes/wmr_default/share/steamvr-monado`. Adjust the command if you have different paths for steam, prefixes.

Now we need to override the head to be assigned to our tracker

If you're using Vive as dongles for other controllers as I did, SteamVR will certainly try to load it first instead of Monado. This might also happen even when using separate dongles, so I would follow these next steps regardless.

Open `~/.steam/steam/config/steamvr.vrsettings` and add this

For vive tracker:

```json
"TrackingOverrides" : {
    "/devices/htc/vive_trackerLHR-SERIAL" : "/user/head"
},
"steamvr" : {
    "activateMultipleDrivers" : true,
    "forcedDriver" : "monado"
}
```

For tundra tracker:

```json
"TrackingOverrides" : {
    "/devices/lighthouse/LHR-SERIAL" : "/user/head"
},
"steamvr" : {
    "activateMultipleDrivers" : true,
    "forcedDriver" : "monado"
}
```

In which you replace `LHR-SERIAL` with your own serial number that you got from previous steps.

`"steamvr"` section should be merged with yours and will allow to activate both lighthouse and monado driver (while monado being primary display).

Be careful with commas in JSON: trailing commas are not allowed but commas are required between entries. If your JSON is invalid, SteamVR will reset it. It's also a good idea to keep backups.

In SteamVR commandline options, set `STEAMVR_EMULATE_INDEX_CONTROLLER=1 %command%` to emulate index controllers instead of empty monado KHR controllers.

## Post-process

Launch SteamVR as usual, and go along with room setup while both headset and tracker are on the ground and visible by base stations. This should recenter them and place onto ground as usual.

Unfortunately the only way to calibrate tracker to hmd in this case is to use [openvr space calibrator](https://github.com/galister/OpenVR-SpaceCalibrator), but there is no documentation on how to do it for 3dof headset being used as display with tracker attached as camera. But you can try editing profile manually, even though it's going to be fairly complicated to calibrate both translation and rotation by yourself. Feel free to add any points on how to do it better or possibly allow this to be done automatically from spacecal app.

Otherwise, it should work now and track the headset using the position data from the tracker.

## Conclusion & Thoughts

At the time of writing this I tested it only with a Vive wand as tracker (see [Turning Vive Wands into Vive Trackers for FULL BODY VR - YouTube](https://www.youtube.com/watch?v=JZrDIvls1ms)), unfortunately requires Windows for flashing devices but it's most likely that you can do it through a virtual machine using USB passthrough (KVM/QEMU or libvirt virtual machines are preferrable for this, VirtualBox can cause irreparable issues when flashing devices via USB passthrough).

What most impressed me in this is colours on that wmr. On Windows, WMR looked very dull and unsaturated to the point i didn't want to use it at all, while there it looks quite vibrant and pretty nice, i actually want to use it now there.

**Used hardware**: Dell Visor VRP 100, Tundra Tracker, 3 steamvr dongles, 2 index knuckle controllers, 2 - 1.0 base stations.

## Additional links

- How to turn tracker into head tracker - [Track Any VR Headset With a Vive Tracker! - YouTube](https://www.youtube.com/watch?v=IgHUqcVUyQA)
