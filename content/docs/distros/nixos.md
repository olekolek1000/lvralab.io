---
title: NixOS
---

# NixOS

General documentation about VR is provided on the [NixOS Wiki](https://wiki.nixos.org/wiki/VR).

## Monado
Monado is supported natively on NixOS using the [services.monado](https://search.nixos.org/options?channel=unstable&query=services.monado) since 24.05.

Make sure to follow the [official guide](https://wiki.nixos.org/wiki/VR#Monado) as it sets the *minimal* requirements needed, then, if those aren't sufficient:

In case of the headset view stuttering, adding `U_PACING_COMP_MIN_TIME_MS = "5";` to `systemd.user.services.monado.environment` could help. Adjust the value as needed.

Similarly, setting the cpu Niceness value to a higher priority manually with `renice -20 -p $(pgrep monado)` could also help. Unfortunately `systemd.user.services.monado.serviceConfig.Nice = -20;` does **not** seem to work.

Once monado is set up the nix way, use `systemctl --user start monado.service` to run it and `systemctl --user stop monado.service` to stop it.

## WiVRn
WiVRn is also supported natively on NixOS using the [services.wivrn](https://search.nixos.org/options?channel=unstable&query=services.wivrn) since 24.05.

As WiVRn is built around Monado, most, if not all, settings for Monado are also available for WiVRn, however you may need to change things like process and option names appropriately.

You can find WiVRn on the official [NixOS wiki](https://wiki.nixos.org/wiki/VR#WiVRn).

### VRChat
To get VRChat working with monado on nix, use the `env PRESSURE_VESSEL_FILESYSTEMS_RW=$XDG_RUNTIME_DIR/monado_comp_ipc %command%` launch options on steam and click play with the "Launch VRChat in Steam VR" mode. It should open it on your desktop for preview and display it through monado.

## SteamVR
SteamVR works like it does on other distros for the most part. Unfotrunately, if it doesn't work out of the box, troubleshooting it on NixOS can be close to impossible due to NixOS's structure and SteamVR's proprietary nature.

[Asynchronous reprojection does not work](https://github.com/NixOS/nixpkgs/issues/217119) without a kernel patch.

[setcap doesn't work](https://github.com/NixOS/nixpkgs/issues/42117#issuecomment-996731579) but can be done manually.

## Envision
[Envision](../../fossvr/envision/) has been packaged in [nixos unstable](https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query=+envision) but is still in early development and is not recommended. It may also mess with your `monado.service` setup in unexpected ways.

## Community Overlays
[Nixpkgs-xr](https://github.com/nix-community/nixpkgs-xr) provides overlays for the exisiting nixpkgs. If the mainline packages are broken for you (whether it be compiling or in function) for any reason, adding this, as described in the readme, might help.

