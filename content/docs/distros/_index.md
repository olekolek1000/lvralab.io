---
weight: 50
title: Distros
---

# Distros

You can read about distribution-specific instructions in this category. Just select your distribution on the sidebar.

Feel free to contribute additional documentation about your distro here!
