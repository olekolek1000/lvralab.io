---
title: OpenSUSE Tumbleweed
---

# OpenSUSE Tumbleweed

**Packages:**
- The OpenXR SDK package can be found in the [hardware:xr](https://build.opensuse.org/package/show/hardware:xr/OpenXR-SDK) [Open Build service](https://en.opensuse.org/openSUSE:Build_Service_FAQ) project. To build [Envision](/docs/fossvr/envision/) profiles, the devel package will also have to be installed.
- The set of [Monado Vulkan Layers](/docs/hardware/) is provided by a [community package](https://build.opensuse.org/package/show/home:Etch-9:xr/monado-vulkan-layers-git).

Any other package in hardware:xr is really outdated and should not be used. Additional information on third-party repositories can be found here: [Additional package repositories (opensuse.org)](https://en.opensuse.org/Additional_package_repositories)