---
weight: 50
title: WiVRn
---

# WiVRn

- [WiVRn GitHub repository](https://github.com/WiVRn/WiVRn)

![The WiVRn mascot](https://github.com/WiVRn/WiVRn/blob/master/images/wivrn.svg?raw=true)

> WiVRn wirelessly connects a standalone VR headset to a Linux computer. You can then play PCVR games on the headset while processing is done on the computer.

WiVRn is based on [Monado](/docs/fossvr/monado/) and can be used with [OpenComposite](/docs/fossvr/opencomposite/) to support a majority of titles available for SteamVR. A wide range of standalone headsets are supported.

## Installing WiVRn

We recommend using [Envision](/docs/fossvr/envision/) to install and launch WiVRn & OpenComposite. Envision will handle all configuration seamlessly and avoids many of the pitfalls of a manual setup.

Alternatively, you can use the [WiVRn Flatpak](https://flathub.org/apps/io.github.wivrn.wivrn) which includes OpenComposite as part of it. Processes in flatpak cannot take advantage of setcap, and so your performance may suffer when compared to Envision.

## General WiVRn Notes

WiVRn uses Avahi for network discovery. Ensure it is running with the following terminal command:
```bash
systemctl enable --now avahi-daemon
```
(If Avahi is not available, the IP address of the server must be entered into the client.)

If a firewall is installed, make sure ports 5353/UDP and 9757/UDP+TCP are open for Avahi and WiVRn itself, respectively.

If using Nvidia proprietary drivers, have [Monado Vulkan Layers](https://gitlab.freedesktop.org/monado/utilities/vulkan-layers) installed, otherwise games will crash with a segmentation fault.

For audio in WiVRn, you will need to assign applications, or the system as a whole, to output audio to the virtual output "WiVRn" which is created upon connection between the server and the headset.

## Wired WiVRn

Use WiVRn with a cable instead of Wifi. Use a 5Gbps cable & port at the very least.

WiVRn version: 0.19+

Using Envision:
- Connect the headset via USB
- Click the `Start WiVRn Client (Wired)` button in Envision.
  - If it's not there, an Envision update might be necessary.

Manual steps:
- Connect the headset via USB
- If WiVRn is running on the headset, close it now.
- While in the system lobby of the headset, run the following `adb` commands on the PC:
  ```bash
  adb reverse tcp:9757 tcp:9757
  adb shell am start -a android.intent.action.VIEW -d "wivrn+tcp://127.0.0.1" org.meumeu.wivrn
  ```

## WiVRn + Lighthouse driver

This section covers using WiVRn with any lighthouse-tracked device: Index/Vive controllers, Vive/Tundra trackers, etc.

WiVRn version: 0.19+

Have SteamVR installed (no need to run it).

In Envision, set WiVRn Profile as such:
- XR Service CMake Flags:
  - `WIVRN_FEATURE_STEAMVR_LIGHTHOUSE=ON`
- Environment Variables:
  - `WIVRN_USE_STEAMVR_LH=1`
  - `LH_DISCOVER_WAIT_MS=6000`

Perform a **Clean Build** after changing the CMake flags!

If not using Envision, simply pass `-DWIVRN_FEATURE_STEAMVR_LIGHTHOUSE=ON` to cmake and export the env variables before starting `wivrn-server`.

[Motoc](/docs/fossvr/motoc/) will be used to calibrate the two tracking technologies to work together.

Discovery happens only on first connection, so be sure to **have all lighthouse devices powered on and in line of sight of base stations before connecting the headset!**

Once a device is discovered, it may be powered off and then back on later.

Once video appears in the headset, check `motoc monitor` to make sure your devices all show up.

If one or more devices are missing, try:
- Spread the devices out more; lighthouse devices get discovered quicker if they're not piled up on each other. Simply strapping them on is good, too.
- Increase `LH_DISCOVER_WAIT_MS`, though this delays the client on first connection.

To re-discover devices, restart WiVRn server.

At this point, your lighthouse devices will be randomly floating about somewhere. To calibrate them, follow the guide in the [Motoc README](https://github.com/galister/motoc/blob/main/README.md).

## WiVRn + SlimeVR trackers

This section covers using WiVRn with SlimeVR trackers (not via OSC). Tracking fidelity is reported to be similar to what's achievable with the SteamVR-native solution.

Required SliveVR Server version: 0.13.0 or later

In Envision, set WiVRn Profile as such:
- XR Service Branch:
  - `master`
- XR Service CMake Flags:
  - `WIVRN_FEATURE_SOLARXR=ON`

Perform a **Clean Build** after changing these!

Starting up:
- Always start SlimeVR server before starting WiVRn.
- Do not quit SlimeVR server mid-session; trackers will not work until WiVRn is restarted.