---
weight: 49
title: Envision
---

# Envision

<video width="640" height="360" controls>
  <source src="/video/envision_installation/envision_installation.webm" type="video/webm">
</video>

- [Envision GitLab repository](https://gitlab.com/gabmus/envision)

Envision is a graphical app that acts as an orchestrator to get a full [Monado](/docs/fossvr/monado/) or [WiVRn](/docs/fossvr/wivrn/) setup up and running with a few clicks.

Envision attempts to construct a working runtime with both a native OpenXR and an OpenVR API, provided by [OpenComposite](/docs/fossvr/opencomposite/), for client aplications to utilize. Please note the OpenVR implementation is incomplete and contains only what's necessary to run most games for compatibility. If you plan to implement software, utilize the OpenXR API, specification [here](https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html).

{{% hint danger %}}
**Warning**

Envision is still considered alpha-quality and highly experimental.
{{% /hint %}}

You can download the latest AppImage snapshot from [GitLab Pipelines](https://gitlab.com/gabmus/envision/-/pipelines?ref=main&status=success).

## Getting Started

Upon launching Envision, you will need to select a profile on the bottom of the left side bar.

Profiles that are available by default:

- Lighthouse driver: Proprietary SteamVR Lighthouse driver with top tracking quality. Recommended for Lighthouse HMDs.
- OpenHMD: Recommended for Oculus CV1 HMDs.
- Simulated headset: Dummy driver for testing on a flat screen.
- Survive: FOSS Lighthouse driver implementation. Lower track quality and less robust than SteamVR proprietary.
- WMR: Use with any SLAM based HMD or Windows Mixed Reality headsets. Inlcudes Rift S support.
- WiVRn: Robust wireless streaming solution for all Android based standalone HMDs.

Monado does not have a launcher app, and so after connecting your headset, you will likely see a solid color. This means you can now start your VR title.

You may want to launch [WlxOverlay-S](/docs/fossvr/wlxoverlay-s/) first and use it to access your desktop and other VR titles. You can even start it automatically alongside the Monado session: just enter `wlx-overlay-s --openxr` into the "Autostart" field of your Envision profile.

## Experimental feature settings

The following resources can be entered into your Envision profile repo and branch settings to enable early access to code before it's fully upstream in Monado itself. To enable these feature sets, simply clone your profile, edit it with these settings, then build.

### Full body Lighthouse tracking

Full body is now provided by default in Monado & OpenComposite through the XR_MNDX_xdev_space OpenXR vendor extension. This allows any tracked "xdev" in Monado to be forwarded as a raw pose without bindings to applications. OpenComposite exposes these as fake Vive FBT trackers for use.

### WMR & Rift S controller tracking

This enables positional tracking for WMR controllers in full 6dof.
Includes Rift S support & controllers.

For the Envision XR Service settings:

- Repo: `https://gitlab.freedesktop.org/thaytan/monado`
- Branch: `dev-constellation-controller-tracking`

### Experimental Pimax support branch

Modest WIP. Allows certain Pimax HMDs to function with Monado. Look Ma, no Pitools!

For the Envision XR Service settings:

- Repo: `https://gitlab.freedesktop.org/Coreforge/monado/`
- Branch: `pimax`

Dumped distortion parameters are found here, please follow the instructions in the README.md and consult with LVRA to add your own if missing.

- https://gitlab.freedesktop.org/othello7/pimax-distortion/

### OpenHMD Rift CV1 support

The Oculus Rift CV1 is supported on the OpenHMD profile of Envision.

Start the profile with your headset placed on the floor in clear view of all bases to generate this file as your calibrated playspace origin first run and delete it to reset the configuration.

A calibration of base stations will be saved to disk at `~/.config/openhmd/rift-room-config.json`.