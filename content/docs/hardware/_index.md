---
weight: 45
title: VR Gear & GPUs
---

# Hardware



## GPU support matrix

| Manufacturer | Driver                    | VR Support     | Reprojection Support           | Hybrid Graphics Support       | Notes                                                                                        |
|--------------|---------------------------|----------------|--------------------------------|-------------------------------|----------------------------------------------------------------------------------------------|
| Nvidia       | Nvidia (Closed Source)    | Excellent      | Excellent                      | Supported                     | Requires an implicit [vulkan-layer](https://gitlab.freedesktop.org/monado/utilities/vulkan-layers) to not segfault: [*AUR*](https://aur.archlinux.org/packages/monado-vulkan-layers-git)・[*jammy*](https://gitlab.freedesktop.org/monado/utilities/vulkan-layers/-/jobs/55337949/artifacts/file/incoming/monado-vulkan-layers_0.9.0.29.git.ae43cdc-1~20240221ubuntu2204_amd64.deb)・[*focal*](https://gitlab.freedesktop.org/monado/utilities/vulkan-layers/-/jobs/55337948/artifacts/file/incoming/monado-vulkan-layers_0.9.0.29.git.ae43cdc-1~20240221ubuntu2004_amd64.deb)・[*bullseye*](https://gitlab.freedesktop.org/monado/utilities/vulkan-layers/-/jobs/55337947/artifacts/file/incoming/monado-vulkan-layers_0.9.0.29.git.ae43cdc-1~20240221bpo11_amd64.deb) |
| Nvidia       | Nouveau (Open Source)     | Functional     | Functional                     | Supported                     | Lacks DisplayPort audio.                                                  |
| Intel        | i915 (Open Source)        | Functional     | Unknown                        | Supported                     | Relatively old, most likely poor support for newer GPUs.
| Intel        | Intel/Xe (Open Source)    | No wired HMDs  | N/A / Unable to drive wired.   | Supported                     | Lacks direct mode implementation in driver, unable to drive wired HMDs.   |
| AMD          | RADV (Open Source)        | Excellent      | Robust (RDNA+)                 | Supported                     | RDNA generation and up supported with compute tunneling for reprojection. Lower than RDNA are not robust. |
| AMD          | AMDVLK (Open Source)      | Use RADV       | Use RADV                       | N/A                           | RADV preferred in all circumstances, unable to drive wired HMDs. Do not use. Do not seek support. |
| AMD          | AMDGPU PRO (Closed Source)| Use RADV       | Use RADV                       | N/A                           | RADV preferred in all circumstances, unable to drive wired HMDs. Do not use. Do not seek support. |

**Notes:**
- **VR Support**: Indicates how well supported the necessary Vulkan API components are.
- **Reprojection Support**: Describes the support and quality of reprojection features for VR. Poor support indicates that the driver is not able to properly handle Vulkan realtime shaders and it will present as visual stutter. Non-robust solutions will suffer stutter under very high GPU load.
- **PRIME/ Hybrid GPU Support**: Compatibility with systems using multiple GPUs for render offload. Monado and all clients must be run on a single select GPU due to memory tiling requirements.
- For Nvidia proprietary drivers, the [vulkan-layer](https://gitlab.freedesktop.org/monado/utilities/vulkan-layers) **must** be installed in order to not crash.
- AMD GPUs lower than RDNA generation have functional but less robust reprojection capabilities, expected to be similar to Intel.
- Audio over displayport is known to temporarily cut out when new audio sources spring up on pipewire [without a fix to add alsa headroom](https://wiki.archlinux.org/title/PipeWire#Audio_cutting_out_when_multiple_streams_start_playing)
- X11 configurations are discouraged but workable, please upgrade your system to Wayland if at all possible.



## XR Devices

A non-comprehensive table of various VR/XR devices and the drivers that support them.

| Device               | [SteamVR](/docs/steamvr/)             | [Monado](/docs/fossvr/monado/) | [WiVRn](/docs/fossvr/wivrn/) |
|----------------------|:-------------------------------------:|:------------------------------:|:----------------------------:|
| **PC VR**            |                                       |                                |                              |
| Valve Index          | ✅                                    | ✅                             | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive             | ✅                                    | ✅                             | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive Pro         | ✅                                    | ✅                             | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive Pro Eye     | ✅ (No eyechip)                       | ✅ (No eyechip)                | 🚧 (WiVRn PC-PC stream)      |
| HTC Vive Pro 2       | ✅ (custom [driver and patches](https://github.com/CertainLach/VivePro2-Linux-Driver)) | ✅ (With two kernel patches [1](https://github.com/CertainLach/VivePro2-Linux-Driver/blob/master/kernel-patches/0002-drm-edid-parse-DRM-VESA-dsc-bpp-target.patch) [2](https://github.com/CertainLach/VivePro2-Linux-Driver/blob/master/kernel-patches/0003-drm-amd-use-fixed-dsc-bits-per-pixel-from-edid.patch), AMD GPUs only.) | --                           |
| Bigscreen Beyond     | ✅ (with [kernel patch](https://gist.github.com/galister/08cddf10ac18929647d5fb6308df3e4b/raw/0f6417b6cb069f19d6c28b730499c07de06ec413/combined-bsb-6-10.patch), AMD GPUs only.)   | ✅ (with [kernel patch](https://gist.github.com/galister/08cddf10ac18929647d5fb6308df3e4b/raw/0f6417b6cb069f19d6c28b730499c07de06ec413/combined-bsb-6-10.patch), AMD GPUs only.)                   | --                           |
| Somnium VR1          | ?                                     | ?                              | ?                            |
| VRgineers XTAL       | ?                                     | ?                              | ?                            |
| StarVR One           | ?                                     | ?                              | ?                            |
| Varjo VR-1           | ?                                     | ?                              | ?                            |
| Varjo VR-2           | ?                                     | ?                              | ?                            |
| Varjo VR-3           | ?                                     | ?                              | ?                            |
| Pimax 4K             | ❌ (Planned)                          | 🚧 (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Pimax 5K Plus        | ❌ (Planned)                          | ✅ (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Pimax 5K XR          | ❌ (Planned)                          | ✅ (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Pimax 5K SUPER       | ❌ (Planned)                          | ✅ (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Pimax 8K             | ❌ (Planned)                          | ✅ (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Pimax Vision 8K X    | ❌ (Planned)                          | ✅ (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Pimax Vision 8K PLUS | ❌ (Planned)                          | ✅ (WIP, with [kernel patches](https://gist.githubusercontent.com/TayouVR/60e3ee5f95375827a66a8898bea02bec/raw/c85135c8d8821ebb2fa85629d837a41de57e12ef/pimax.patch)) | 🚧 (WiVRn PC-PC stream)          |
| Lenovo Explorer      | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Acer AH101           | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Dell Visor           | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| HP WMR headset       | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Samsung Odyssey      | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Asus HC102           | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Samsung Odyssey+     | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| HP Reverb            | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Acer OJO 500         | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| HP Reverb G2         | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| Oculus Rift CV1      | ✅ (OpenHMD SteamVR plugin)           | ✅ (OpenHMD plugin based support)  | 🚧 (WiVRn PC-PC stream)      |
| Oculus Rift S        | ✅ (Monado SteamVR plugin)            | ✅ (experimental 6dof controllers) | 🚧 (WiVRn PC-PC stream)      |
| **Standalone**       |                                       |                                |                              |
| Quest                | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest 2              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest Pro            | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Quest 3              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Pico 4               | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Pico Neo 3           | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| HTC Vive Focus 3     | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| HTC Vive XR Elite    | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Lynx R1              | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | ✅                           |
| Apple Vision Pro     | ✅ (via [ALVR](/docs/steamvr/alvr/))  | --                             | 🚧 (WIP client)              |
| **Trackers**         |                                       |                               |                              |
| Vive/Tundra trackers | ✅ (native or spacecal)               | ✅ (native or motoc)           | ✅ (motoc)                   |
| SlimeVR trackers     | ✅                                    | ✅ (OSC + experimental driver) | ✅                           |
| Project Babble       | ✅ (oscavmgr)                         | ✅ (oscavmgr)                  | ✅ (oscavmgr)                |
| Eyetrack VR          | ✅ (oscavmgr)                         | ✅ (oscavmgr)                  | ✅ (oscavmgr)                |
| Mercury Handtrack    | 🚧 (Monado SteamVR plugin, win only)  | ✅ (survive driver only)       | ❌                           |
| Lucid VR Gloves      | ?                                     | ✅ (survive driver only)       | ❌                           |
| Kinect FBT           | ✅                                    | ✅ (experimental)              | 🚧                           |
| Standable FBT        | ❌                                    | ❌                             | ❌                           |

## Hardware note

WiVRn PC XR to WiVRn PC client streaming remains mainly for debug use.

Vive Pro microphones should be set to use 44.1khz sample rates as feeding in 48khz raises the pitch of the audio.

Vive Pro creates HDMI Output Source after startup of SteamVR/Monado/etc. Use it instead of Vive Pro USB Audio Source, since the audio might appear distorted.

Valve index audio output should be set to 48khz or no audio will output.

Vive Pro Eye HMD functional, eyechip WIP.

Pimax initialization code WIP. Distortion matrix dump work in progress.

Eyetrack VR and Project Babble will both be implemented through [oscavmgr](https://github.com/galister/oscavmgr) to emit proper unified flexes over OSC.

Tracking technologies can be mixed Monado/WiVRn by using [motoc](https://github.com/galister/motoc).

## Applying a kernel patch (for Vive Pro 2, Bigscreen Beyond, Pimax)

### Arch

1. download the patch(es), then follow the steps on https://wiki.archlinux.org/title/Kernel/Arch_build_system,
    which should be roughly: `mkdir ~/build/` -> `cd ~/build/` -> `pkgctl repo clone --protocol=https linux` -> `cd linux`
2. (section 2 of link) open `PKGBUILD` in that directory and modify the `pkgbase` line (at the top) to read e.g. "`pkgbase=linux-customvr`". then add a line containing "`<patchfilename>.patch`" to the `source` array:

```pkgbuild
source=(
  https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/${_srcname}.tar.{xz,sign}
  <patchfilename>.patch
  $url/releases/download/$_srctag/linux-$_srctag.patch.zst{,.sig}
  config  # the main kernel config file
)
```

4. close `PKGBUILD`. (may not be necessary but i ran `updpkgsums`) -> `makepkg -s --skippgpcheck` (i didn't feel like fixing a pgp error)
5. then section 4 of https://wiki.archlinux.org/title/Kernel/Arch_build_system#Installing and restart

### Fedora

1. Prepare the kernel
  ```sh
  git clone https://src.fedoraproject.org/rpms/kernel.git
  cd kernel

  # replace 41 with your fedora version
  git switch f41

  # name this the 'bsb' build
  sed -i 's/# define buildid .*/%define buildid .bsb/g' kernel.spec
  ```

2. Replace `linux-kernel-test.patch` with the actual patch file downloaded from the table above

3. Build RPMs:
  ```sh
  fedpkg local
  ```

4. Install kernel:
  ```sh
  sudo dnf install --nogpgcheck ./x86_64/kernel-*.rpm
  ```
5. Reboot and check that using `uname -a` that the `bsb` kernel is running.

Official guide: [Building a Kernel from the Fedora dist-git](https://docs.fedoraproject.org/en-US/quick-docs/kernel-build-custom/#_building_a_kernel_from_the_fedora_dist_git)

### NixOS

1. download the patch(es)
2. add this to your configuration.nix or other nix file you use:
```nix
  boot.kernelPatches = [
    {
      name = "type what the patch is for here";
      patch = /path/to/patch/file.patch;
    }
  ];
```
reference commit on my nix files: https://github.com/TayouVR/nixfiles/commit/d6ef568a2642c5a26eb92611be7335afdb6047de
