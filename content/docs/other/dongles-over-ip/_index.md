---
weight: 300
title: Dongles over IP
---

# Dongles over IP

This setup uses USB-over-IP to let you plug your Watchman dongles (the ones that comes with Tundra / Vive Trackers) into a different computer (like a Raspberry Pi). These dongles can then be used with trackers as well as index/vive/pimax/etc controllers.

#### The benefits of this setup

- Dongles can be far away from your computer
- Dongles can be far away from radio interference
- You can conveniently hide the ugly cabling under/behind a piece of furniture

#### Requirements

- Main PC connected to the same wired network as the dongle host. While you can technically use WiFi, it's not recommended as it has a high chance to interfere with the Bluetooth connection of dongles.

### Dongle host setup

1. Install `usbip` and `python3` on dongle host. On Debian/Raspbian it's `apt install usbip python3`.
2. Put the `usbip-bind` script (from below) on the dongle host, (e.g. into `/root`)
`sudo wget https://gist.github.com/galister/2fbd23cfaff8ba4839efa13184e4e197/raw/13b0b2fd6dee609169b6a98b302f6a6ac5c2d307/usbip-bind -O /root/usbip-bind; sudo chmod +x /root/usbip-bind`
3. Set up `usbip-bind bind` to run on startup. On Debian/Raspbian: `echo '/root/usbip-bind bind' | sudo tee -a /etc/rc.local`
4. With your dongles plugged in, run `sudo /root/usbip-bind bind`, to bind them for the first time

### Main PC setup:

1. Download `usbdongle` and put it somewhere safe:
`wget https://gist.github.com/galister/2fbd23cfaff8ba4839efa13184e4e197/raw/13b0b2fd6dee609169b6a98b302f6a6ac5c2d307/usbdongle; chmod +x usbdongle`
2. Edit `usbdongle` and set the `DONGLE_HOST` variable to the IP or hostname of your dongle host. (run `ip addr` on dongle host to find out)
3. Run `usbdongle` before starting SteamVR/Monado (or on startup/login)

### Things to watch out for

- **Whenever a new dongle was plugged in** or an existing re-plugged, run `sudo /root/usbip-bind rebind` on the dongle host
- **If the dongle host is rebooted**, stop steamvr/monado, run `usbdongle` and then re-start SteamVR/Monado.
- In case there's a network issue and `usbdongle` can't rebind even after running `usbip-bind rebind` on dongle host, try rebooting both hosts.
- By default, the Linux kernel supports up to 8 virtual USB devices.
    - If you need more than 8, you can change the `CONFIG_USBIP_VHCI_HC_PORTS` kernel config option and re-compile your kernel.
- A Pi 3.0 Model A is known to handle 2x Tundra SW4 dongles or 8 Vive dongles without requiring a powered hub

## Scripts

You can find the original ones [here](https://gist.github.com/galister/2fbd23cfaff8ba4839efa13184e4e197).

## Support

Reach out to @galister on Discord/Matrix